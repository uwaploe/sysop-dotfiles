# -*- mode: shell-script -*-

if [[ $TERM = dumb ]]; then
    export PS1='\$ '
else
    export PS1='\[\033[1;32m\][\u@\h:\w]\$\[\033[0m\] '
fi
export PYTHONPATH=$HOME/.nix-profile/lib/python3.6/site-packages:$PYTHONPATH
export PKG_CONFIG_PATH=$HOME/.nix-profile/share/pkgconfig:$PKG_CONFIG_PATH

if [[ -e ~/.bashrc ]]; then
    . ~/.bashrc
fi

systemctl --user import-environment PATH

[[ -e ~/venv/bin/activate ]] && . ~/venv/bin/activate 2> /dev/null
