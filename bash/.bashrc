# -*- mode: shell-script -*-

# Set GOPATH
for p in $NIX_PROFILES; do
    GOPATH="$p/share/go:$GOPATH"
done
[[ -d $HOME/GO ]] && GOPATH=$HOME/GO:$GOPATH

# Quit if shell is non-interactive
[[ -z "$PS1" ]] && return

alias po=popd
alias pu=pushd
alias d=dirs
alias rm='rm -i --one-file-system'
