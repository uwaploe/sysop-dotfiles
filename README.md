# COVIS Dotfiles

This repository contains various configuration files for the system
operator account (sysop) on the COVIS System Interface Computer
(SIC). Clone this repository to the directory `~/dotfiles` on the COVIS
SIC then run:

``` shellsession
cd ~/dotfiles
./install.sh
```
